#include <iostream>
#include <fstream>
#include <random>
#include <cmath>
#include <vector>
using namespace std;
// Pseudo-Random Normal Variable
random_device rd;
mt19937 gen(rd());
normal_distribution<> d(0,1);

// Parameters
const double dt=0.001;
const int NbIt=1000/dt;
const int NbSampling=NbIt*dt/10;
const double beta=1;//10;

void schema(long double &q, long double &p, double gamma, double dt, double beta){
	long double alpha=exp(-gamma*dt/2.0);
		p = alpha * p + sqrt( (1-alpha*alpha)/beta) * d(gen);
		p = p - dt/2.0 * sin(q);
		q = q + dt * p;
		p = p - dt/2.0 * sin(q);
		p = alpha * p + sqrt( (1-alpha*alpha)/beta) * d(gen);	
}

int main() {
	for(int i_g=5; i_g<=5; i_g++){
		// Initialisation
		long double q; // position
		long double p;   // velocity
		long double q0=0;
		long double p0=0;
		double gamma=1.0*pow(4,i_g);			// Gamma value
		vector<long double> q_data, p_data, q_average;  
		vector<long double> H;
		//long double H_temp=0;   // Initial hamiltonian's value
		for(int i=0; i<NbIt/NbSampling; i++){
			q_data.push_back(0);
			q_average.push_back(0);
		}
		int maxIter = 10000;
		for(int iter=0; iter < maxIter; iter++){ 
			q=q0;
			p=p0;
			if(iter % (maxIter/100) == 0){
			    // Print loading bar
			    float ratio = (iter /(float) maxIter);
			    int   c     = ratio * 100;
			    printf("%3d%% [", (int)(ratio*100) );
			    for (int x=0; x<c; x++)
			      printf("#");
			    for (int x=c; x<100; x++)
			      printf("-");
			    printf("]\n\033[F\033[J");
			}
			// Iterations
			for(int i=0; i<NbIt; i++){
				//H_temp=0;
				schema(q,p,gamma,dt,beta);  // call the function schema to compute new values for p and q.
				if(i % NbSampling == 0){  
					q_data[i/NbSampling]+=(q-q0)/maxIter;    // add the computed value of q
					q_average[i/NbSampling]+=(q-q0)*(q-q0)/maxIter; // add the square value to compute the average
					//p_data.push_back(p);    // insert the computed value of p

					//H_temp += (p*p/2.0+(1-cos(q))); // compute the new hamiltonian's value
					//H.push_back(H_temp);        // insert new hamiltonian's value in an vector
				// We display the last values of the current iteration
					//cout << "q = " << q << endl;
					//cout << "p = " << p << endl;
					//cout << "H = " << H_temp << endl;
				//cout << q << "   " << p << endl; 
				}
			}
			// We put the new initial values 
			q0 = q; // - floor(q);
			p0 = p;

			
			/*ofstream file1("results_p.csv", ios::out | ios::app);  
			if(file1) 
			{
				for(int i=0; i<p_data.size(); i++){
					file1 << p_data[i] << endl;
				}
				file1.close();
				//cout << "File results_p.csv written" << endl;
			}else 
				cerr << "File not opened!" << endl;
			*/	
		/*	ofstream file2("results_H.csv", ios::out | ios::app);  
			if(file2) 
			{
				for(int i=0; i<H.size(); i++){
					file2 << H[i] << endl;
				}
				file2.close();
			}else 
				cerr << "File not opened!" << endl;*/
			//q_data.clear();
			//p_data.clear();
			//H.clear();
			
			
		}
		// Save results
		string file_suffix = to_string(i_g);
		string filename="results_q_check_" + file_suffix + ".csv";
		string filename1="results_q_average_check_" + file_suffix + ".csv";
		ofstream file(filename, ios::out | ios::trunc);  
		if(file) 
		{
			for(int i=0; i<q_data.size(); i++){
				file << q_data[i] << endl;
			}
			file.close();
			cout << "File " << filename << " written" << endl;
		}else 
			cerr << "File not opened!" << endl;	
		ofstream file2(filename1, ios::out | ios::trunc);  
		if(file2) 
		{
			for(int i=0; i<q_data.size(); i++){
				file2 << q_average[i] << endl;
			}
			file2.close();
			cout << "File " << filename1 << " written" << endl;
		}else 
			cerr << "File not opened!" << endl;
	}
	return 0;
}
