import csv
import numpy as np
results=np.zeros((11,1000)) #[[0 for i in range(1000)] for j in range(-5,6)]
with open('results_q_average_-5.csv') as csv_:
    results_=csv.reader(csv_)
    i=0
    for row in results_:
        if i==0:
            results[0][i]=float(row[0])
        else:
            results[0][i]=float(row[0])/(2.0*i)
        i+=1
with open('results_q_average_-4.csv') as csv_:
    results_=csv.reader(csv_)
    i=0
    for row in results_:
        if i==0:
            results[1][i]=float(row[0])
        else:
            results[1][i]=float(row[0])/(2.0*i)
        i+=1
with open('results_q_average_-3.csv') as csv_:
    results_=csv.reader(csv_)
    i=0
    for row in results_:
        if i==0:
            results[2][i]=float(row[0])
        else:
            results[2][i]=float(row[0])/(2.0*i)
        i+=1
with open('results_q_average_-2.csv') as csv_:
    results_=csv.reader(csv_)
    i=0
    for row in results_:
        if i==0:
            results[3][i]=float(row[0])
        else:
            results[3][i]=float(row[0])/(2.0*i)
        i+=1
with open('results_q_average_-1.csv') as csv_:
    results_=csv.reader(csv_)
    i=0
    for row in results_:
        if i==0:
            results[4][i]=float(row[0])
        else:
            results[4][i]=float(row[0])/(2.0*i)
        i+=1
with open('results_q_average_0.csv') as csv_:
    results_=csv.reader(csv_)
    i=0
    for row in results_:
        if i==0:
            results[5][i]=float(row[0])
        else:
            results[5][i]=float(row[0])/(2.0*i)
        i+=1
with open('results_q_average_1.csv') as csv_:
    results_=csv.reader(csv_)
    i=0
    for row in results_:
        if i==0:
            results[6][i]=float(row[0])
        else:
            results[6][i]=float(row[0])/(2.0*i)
        i+=1       
with open('results_q_average_2.csv') as csv_:
    results_=csv.reader(csv_)
    i=0
    for row in results_:
        if i==0:
            results[7][i]=float(row[0])
        else:
            results[7][i]=float(row[0])/(2.0*i)
        i+=1       
with open('results_q_average_3.csv') as csv_:
    results_=csv.reader(csv_)
    i=0
    for row in results_:
        if i==0:
            results[8][i]=float(row[0])
        else:
            results[8][i]=float(row[0])/(2.0*i)
        i+=1       
with open('results_q_average_4.csv') as csv_:
    results_=csv.reader(csv_)
    i=0
    for row in results_:
        if i==0:
            results[9][i]=float(row[0])
        else:
            results[9][i]=float(row[0])/(2.0*i)
        i+=1       
with open('results_q_average_5.csv') as csv_:
    results_=csv.reader(csv_)
    i=0
    for row in results_:
        if i==0:
            results[10][i]=float(row[0])
        else:
            results[10][i]=float(row[0])/(2.0*i)
        i+=1       
with open('results_average_all.csv','w') as csv_:
    mywriter=csv.writer(csv_)
    for row in np.transpose(results):
        mywriter.writerow(row)
import statistics
D=np.zeros(11)
stDev=np.zeros(11)
for i in range(11):
    D[i]=statistics.mean(results[i][100:])
    stDev[i]=statistics.stdev(results[i][100:])
    print("\nGamma = "+str(4**(i-5))+"\nD = "+str(D[i])+"\ns = "+str(stDev[i]))
